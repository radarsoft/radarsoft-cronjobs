FROM docker.io/denoland/deno:alpine-1.45.4
WORKDIR /radarsoft-cronjobs
COPY imports.json ./
COPY lib ./lib
