import { GraphQl } from "graphql";
import { createLazyClient, Redis } from "redis";
import { GraphQlInit, RedisInit } from "types";

export class RedisSyncer {
  private _gqlClient: GraphQl;
  private _redisClient: Redis;

  constructor(gqlInit: GraphQlInit, redisInit: RedisInit) {
    this._gqlClient = new GraphQl(
      gqlInit.url,
      "X-Hasura-Admin-Secret",
      gqlInit.auth,
    );
    this._redisClient = createLazyClient(redisInit);
  }

  public async sync(): Promise<void | never> {
    await this._syncToPost();
    await this._syncToSave();
  }

  private async _syncToPost(): Promise<void> {
    const toPostResponse = await this._gqlClient.getToPost();
    await this._redisClient.set(
      "to_post",
      toPostResponse.queue_entry_aggregate.aggregate.count,
    );
  }

  private async _syncToSave(): Promise<void> {
    const toSaveResponse = await this._gqlClient.getToSave();
    await this._redisClient.set(
      "to_save",
      toSaveResponse.queue_entry_aggregate.aggregate.count,
    );
  }
}
