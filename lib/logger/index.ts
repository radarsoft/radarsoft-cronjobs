import { ConsoleHandler } from "log/handlers";
import { Logger } from "log/logger";

export class CronjobsLogger extends Logger {
  constructor(loggerName: string) {
    super(loggerName, "NOTSET", {
      handlers: [
        new ConsoleHandler("DEBUG", {
          formatter: (logResult) => {
            return `${logResult.datetime.toISOString()} - DEBUG [${logResult.loggerName}] ${logResult.msg}`;
          },
        }),
        new ConsoleHandler("INFO", {
          formatter: (logResult) => {
            return `${logResult.datetime.toISOString()} - [${logResult.loggerName}] ${logResult.msg}`;
          },
        }),
        new ConsoleHandler("CRITICAL", {
          formatter: (logResult) => {
            return `${logResult.datetime.toISOString()} - CRITICAL [${logResult.loggerName}] ${logResult.msg}`;
          },
        }),
      ],
    });
  }
}
