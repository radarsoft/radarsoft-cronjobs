import { AggregateCount } from "./AggregateCount.ts";

export type QueueEntryAggregate = {
  queue_entry_aggregate: AggregateCount;
};
