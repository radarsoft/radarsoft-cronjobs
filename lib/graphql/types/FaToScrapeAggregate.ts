import { AggregateCount } from "./AggregateCount.ts";

export type FaToScrapeAggregate = {
  fa_to_scrape_aggregate: AggregateCount;
};
