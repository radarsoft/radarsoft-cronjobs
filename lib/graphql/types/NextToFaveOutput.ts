import { PostOrigin } from "types/graphql";

export type NextToFaveOutput = {
  queue_entry: {
    id: number;
    origin: PostOrigin;
    post_id: number;
  }[];
};
