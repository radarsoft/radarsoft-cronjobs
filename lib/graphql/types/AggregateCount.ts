export type AggregateCount = {
  aggregate: {
    count: number;
  };
};
