export type RemoveToScrapeOutput = {
  delete_fa_to_scrape: {
    affected_rows: number;
  };
};
