import { PostOrigin } from "types/graphql";

export type InsertToScrapeInput = {
  post_id: number;
  origin: PostOrigin;
  origin_id_comb: string;
};
