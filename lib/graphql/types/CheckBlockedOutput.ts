export type CheckBlockedOutput = {
  fa_blocked: {
    artist_name: string;
  }[];
};
