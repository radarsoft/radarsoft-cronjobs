export type NextEntryOutput = {
  get_next_post: {
    id: number;
    full_link: string;
    artist_link: string;
    artist_name?: string;
    post_link: string;
    post_name: string;
    tip_link?: string;
    tg_image_link: string;
    origin: string;
  }[];
};
