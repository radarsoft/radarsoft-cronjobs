export type CheckExistingOutput = {
  queue_entry: {
    id: number;
  }[];
};
