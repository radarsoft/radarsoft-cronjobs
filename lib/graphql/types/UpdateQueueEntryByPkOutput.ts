export type UpdateQueueEntryByPkOutput = {
  update_queue_entry_by_pk: {
    id: number;
  };
};
