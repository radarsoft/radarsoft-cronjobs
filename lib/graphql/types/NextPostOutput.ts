import { PostOrigin } from "types/graphql";

export type NextPostOutput = {
  fa_to_scrape: {
    origin: PostOrigin;
    post_id: number;
    ignore_reposter: boolean;
  }[];
};
