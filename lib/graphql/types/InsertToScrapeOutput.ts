export type InsertToScrapeOutput = {
  insert_fa_to_scrape: {
    affected_rows: number;
  };
};
