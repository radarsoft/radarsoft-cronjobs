import {
  CheckBlockedInput,
  CheckBlockedOutput,
  CheckExistingInput,
  CheckExistingOutput,
  CheckReposterInput,
  CheckReposterOutput,
  FaToScrapeAggregate,
  InsertEntryInput,
  InsertEntryOutput,
  InsertToScrapeInput,
  InsertToScrapeOutput,
  NextEntryOutput,
  NextPostOutput,
  NextToFaveOutput,
  QueueEntryAggregate,
  RemoveToScrapeOutput,
  UpdateQueueEntryByPkOutput,
} from "types/graphql";

export class GraphQl {
  private _url: string;
  private _defaultConfig: Partial<RequestInit>;

  private _operations = `
        mutation InsertToScrape($objects: [fa_to_scrape_insert_input!]) {
          insert_fa_to_scrape(objects: $objects) {
            affected_rows
          }
        }

        mutation RemoveToScrape($_eq: String!) {
          delete_fa_to_scrape(where: { origin_id_comb: { _eq: $_eq } }) {
            affected_rows
          }
        }

        query GetNextToScrape {
          fa_to_scrape(limit: 1, order_by: { post_id: asc }) {
            origin
            post_id,
            ignore_reposter
          }
        }

        query GetNextNonFaToScrape {
          fa_to_scrape(limit: 1, where: { origin: { _neq: "FA" } }, order_by: { post_id: asc }) {
            post_id
            origin
            ignore_reposter
          }
        }

        query CheckExisting($postKey: String!) {
          queue_entry(where: { post_origin_id_comb: { _eq: $postKey } }) {
            id
          }
        }

        query CheckReposter($name: String!) {
          fa_reposters(where: {
            artist_name: { _eq: $name }
          }) {
            artist_name
          }
        }

        mutation InsertEntry($profile: String!, $profileName: String!, $full: String!, $postId: Int!, $link: String!, $title: String!, $postKey: String!, $apiVersion: String!, $thumbnail: String, $origin: String!) {
          insert_queue_entry_one(object: {
            artist_link: $profile,
            artist_name: $profileName,
            full_link: $full,
            origin: $origin,
            post_id: $postId,
            post_link: $link,
            post_name: $title,
            post_origin_id_comb: $postKey,
            posted: false,
            saved_on_disk: false,
            saved_with_api_ver: $apiVersion,
            tg_image_link: $thumbnail
          }) {
            id
          }
        }

        query CheckBlocked($profileName: String!) {
          fa_blocked(where: {
            artist_name: { _eq: $profileName }
          }) {
            artist_name
          }
        }

        query GetNextEntry {
          get_next_post {
            id
            full_link
            artist_link
            artist_name
            post_link
            post_name
            tip_link
            tg_image_link
            origin
          }
        }

        mutation SetPosted($id: Int!) {
          update_queue_entry_by_pk(pk_columns: { id: $id }, _set: { posted: true }) {
            id
          }
        }

        query NotPosted {
          queue_entry_aggregate(where: { posted: { _eq: false } }) {
            aggregate {
              count
            }
          }
        }

        query NotSaved {
          queue_entry_aggregate(where: { saved_on_disk: { _eq: false } }) {
            aggregate {
              count
            }
          }
        }

        mutation UpdateApiVer($id: Int!, $apiVer: String!) {
          update_queue_entry_by_pk(pk_columns: { id: $id }, _set: { saved_with_api_ver: $apiVer }) {
            id
          }
        }

        mutation SetFaved($id: Int!) {
          update_queue_entry_by_pk(pk_columns: { id: $id }, _set: { faved: true }) {
            id
          }
        }

        query GetNextToFave {
          queue_entry(limit: 1, order_by: { id: asc }, where: { faved: { _eq: false } }) {
            id
            origin
            post_id
          }
        }

        query CheckNonFaToScrape {
          fa_to_scrape_aggregate(where: { _not: { origin: { _eq: "FA" } } }) {
            aggregate {
              count
            }
          }
        }
    `;

  constructor(url: string, authHeaderKey?: string, authHeaderValue?: string) {
    this._url = url;

    const configHeaders = new Headers({
      "Content-Type": "application/json",
    });

    if (authHeaderKey && authHeaderValue) {
      configHeaders.append(authHeaderKey, authHeaderValue);
    }

    this._defaultConfig = {
      method: "POST",
      headers: configHeaders,
    };
  }

  private async _fetchGql<T>(
    operationName: string,
    variables?: Record<string, unknown>,
  ): Promise<T> {
    const currentRequest = {
      ...this._defaultConfig,
      body: JSON.stringify({
        query: this._operations,
        variables,
        operationName,
      }),
    };

    type ResultResponseData = {
      data: T;
    };

    const resultRaw = await fetch(this._url, currentRequest);
    const { data } = (await resultRaw.json()) as ResultResponseData;
    return data;
  }

  public insertToScrape(
    objects: InsertToScrapeInput[],
  ): Promise<InsertToScrapeOutput> {
    return this._fetchGql<InsertToScrapeOutput>("InsertToScrape", { objects });
  }

  public deleteToScrape(originIdComb: string): Promise<RemoveToScrapeOutput> {
    return this._fetchGql<RemoveToScrapeOutput>("RemoveToScrape", {
      _eq: originIdComb,
    });
  }

  public getNextToScrape(): Promise<NextPostOutput> {
    return this._fetchGql<NextPostOutput>("GetNextToScrape");
  }

  public getNextNonFaToScrape(): Promise<NextPostOutput> {
    return this._fetchGql<NextPostOutput>("GetNextNonFaToScrape");
  }

  public checkExisting(obj: CheckExistingInput): Promise<CheckExistingOutput> {
    return this._fetchGql<CheckExistingOutput>("CheckExisting", obj);
  }

  public checkReposter(obj: CheckReposterInput): Promise<CheckReposterOutput> {
    return this._fetchGql<CheckReposterOutput>("CheckReposter", obj);
  }

  public insertEntry(obj: InsertEntryInput): Promise<InsertEntryOutput> {
    return this._fetchGql<InsertEntryOutput>("InsertEntry", obj);
  }

  public checkBlocked(obj: CheckBlockedInput): Promise<CheckBlockedOutput> {
    return this._fetchGql<CheckBlockedOutput>("CheckBlocked", obj);
  }

  public getNextEntry(): Promise<NextEntryOutput> {
    return this._fetchGql<NextEntryOutput>("GetNextEntry");
  }

  public setPosted(id: number): Promise<UpdateQueueEntryByPkOutput> {
    return this._fetchGql<UpdateQueueEntryByPkOutput>("SetPosted", { id });
  }

  public getToPost(): Promise<QueueEntryAggregate> {
    return this._fetchGql<QueueEntryAggregate>("NotPosted");
  }

  public getToSave(): Promise<QueueEntryAggregate> {
    return this._fetchGql<QueueEntryAggregate>("NotSaved");
  }

  public updateApiVer(id: number): Promise<UpdateQueueEntryByPkOutput> {
    return this._fetchGql<UpdateQueueEntryByPkOutput>("UpdateApiVer", {
      id,
      apiVer: (Deno.env.get("cronjobs-version") || "3.0.0"),
    });
  }

  public setFaved(id: number): Promise<UpdateQueueEntryByPkOutput> {
    return this._fetchGql<UpdateQueueEntryByPkOutput>("SetFaved", { id });
  }

  public getNextToFave(): Promise<NextToFaveOutput> {
    return this._fetchGql<NextToFaveOutput>("GetNextToFave");
  }

  public checkNonFaToScrape(): Promise<FaToScrapeAggregate> {
    return this._fetchGql<FaToScrapeAggregate>("CheckNonFaToScrape");
  }
}
