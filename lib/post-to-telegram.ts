import { TelegramPoster } from "./PostToTelegram/index.ts";

const GRAPHQL_URL = Deno.env.get("GQL_URL");
const GRAPHQL_AUTH = Deno.env.get("GQL_AUTH");
const TG_API_URL = Deno.env.get("BASE_API_URL");
const CHANNEL_ID = Deno.env.get("TG_CHANNEL_ID");
const BOT_TOKEN = Deno.env.get("BOT_API_TOKEN");

if (!GRAPHQL_URL || !GRAPHQL_AUTH || !TG_API_URL || !CHANNEL_ID || !BOT_TOKEN) {
  console.error("One or more of the required env variables is unset!");
  Deno.exit(1);
}

const poster = new TelegramPoster(CHANNEL_ID, {
  url: GRAPHQL_URL,
  auth: GRAPHQL_AUTH,
}, {
  url: TG_API_URL,
  token: BOT_TOKEN,
});

await poster.post();
