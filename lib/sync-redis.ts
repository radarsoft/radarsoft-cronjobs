import { RedisSyncer } from "./SyncRedis/index.ts";

const REDIS_HOST = Deno.env.get("redis-hostname");
const REDIS_PORT = parseInt(Deno.env.get("redis-port") || "6379");
const REDIS_PASSWORD = Deno.env.get("redis-password");

if (!REDIS_HOST || !REDIS_PASSWORD) {
  console.error("Redis ENV not set, cannot run without it!");
  Deno.exit(1);
}

const GQL_URL = Deno.env.get("GQL_URL");
const GQL_AUTH = Deno.env.get("GQL_AUTH");

if (!GQL_URL || !GQL_AUTH) {
  console.error("GraphQL ENV not set, cannot run without it!");
  Deno.exit(1);
}

const syncer = new RedisSyncer({ url: GQL_URL, auth: GQL_AUTH }, {
  hostname: REDIS_HOST,
  port: REDIS_PORT,
  password: REDIS_PASSWORD,
});

await syncer.sync();
