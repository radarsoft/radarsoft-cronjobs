import { Api } from "grammy";
import { GraphQl } from "graphql";
import { createLazyClient, Redis } from "redis";

import {
  GraphQlInit,
  OnlineUsers,
  ScrapeErrorResult,
  ScrapeResult,
  TelegramApiInit,
  WeasylResult,
} from "types";

import {
  CheckExistingOutput,
  InsertEntryInput,
  NextPostOutput,
  PostOrigin,
  RemoveToScrapeOutput,
} from "types/graphql";
import { CheckReposterOutput } from "../graphql/types/CheckReposterOutput.ts";
import { CronjobsLogger } from "../logger/index.ts";

export class QueueProcessor {
  private _gqlClient: GraphQl;
  private _tgApiClient: Api;
  private _scraperUrl: string;
  private _apiVersion: string;
  private _userId: string;
  private _faCookie: string;
  private _e6Username: string;
  private _e6ApiKey: string;
  private _wsApiKey: string;
  private _retryCount = 0;
  private _logger: CronjobsLogger;
  private _redisClient?: Redis;

  constructor(
    gqlInit: GraphQlInit,
    tgInit: TelegramApiInit,
    scraperUrl: string,
    faCookie: string,
    apiVersion: string,
    userId: string,
    e6Username: string,
    e6ApiKey: string,
    wsApiKey: string,
  ) {
    this._logger = new CronjobsLogger("ProcessQueue");
    this._scraperUrl = scraperUrl;
    this._userId = userId;
    this._apiVersion = apiVersion;
    this._faCookie = faCookie;
    this._e6Username = e6Username;
    this._e6ApiKey = e6ApiKey;
    this._wsApiKey = wsApiKey;

    this._tgApiClient = new Api(tgInit.token, {
      apiRoot: tgInit.url,
    });

    this._gqlClient = new GraphQl(
      gqlInit.url,
      "X-Hasura-Admin-Secret",
      gqlInit.auth,
    );

    const REDIS_HOST = Deno.env.get("redis-hostname");
    const REDIS_PORT = parseInt(Deno.env.get("redis-port") || "6379");
    const REDIS_PASSWORD = Deno.env.get("redis-password");
    if (!REDIS_HOST || !REDIS_PASSWORD) {
      this._logger.error(
        "Redis hostname and/or password not set, will NOT use redis.",
      );
    } else {
      this._redisClient = createLazyClient({
        hostname: REDIS_HOST,
        port: REDIS_PORT,
        password: REDIS_PASSWORD,
      });
    }
  }

  private async _deleteToScrape(originIdCombo: string): Promise<void | never> {
    this._logger.info(`Trying to delete origin_id_comb: ${originIdCombo}.`);
    let deleteResponse: RemoveToScrapeOutput;
    try {
      deleteResponse = await this._gqlClient.deleteToScrape(originIdCombo);
    } catch (e) {
      this._logger.critical(
        `Error deleting PostID ${originIdCombo}, delete manually. Failure.`,
      );
      this._logger.debug(e);

      Deno.exit(1);
    }

    this._logger.info("Post deleted.");
    this._logger.debug(JSON.stringify(deleteResponse, undefined, 2));
  }

  private async _getStatus(): Promise<number> {
    const result = await fetch(`${this._scraperUrl}/status`).then(async (res) =>
      (await res.json()) as OnlineUsers
    );

    return parseInt(`${result.registered}`, 10);
  }

  private async _favInsteadOfScrape(): Promise<void | never> {
    const nextPost = await this._gqlClient.getNextToFave();

    if (nextPost.queue_entry.length !== 1) {
      return;
    }

    const { id, post_id, origin } = nextPost.queue_entry[0];

    if (origin === "SF" || origin === "KE") {
      await this._gqlClient.setFaved(id);
      return;
    }

    const postData = await this._scrapePost(origin, post_id);

    if (postData === 0) {
      return;
    }

    if (postData === 404) {
      await this._gqlClient.setFaved(id);
      return;
    }

    if (origin === "FA") {
      const isBlocked = await this._checkBlocked(postData.profile_name);
      this._logger.debug("isBlocked result:", isBlocked);
      if (isBlocked) {
        this._logger.info(
          `Artist ${postData.profile_name} is blocked. Skipping favorite.`,
        );
        await this._gqlClient.setFaved(id);
        return;
      }

      if (postData.fav_status) {
        this._logger.info(
          `Post FA_${post_id} is already faved. Skipping favorite.`,
        );

        await this._gqlClient.setFaved(id);
        return;
      }
    }

    await this._favorite(origin, post_id, id, postData.fav_key);
  }

  private async _getNextPost(
    onlyNonFa: boolean,
  ): Promise<NextPostOutput | never> {
    try {
      const result = onlyNonFa
        ? await this._gqlClient.getNextNonFaToScrape()
        : await this._gqlClient.getNextToScrape();
      if (result.fa_to_scrape.length !== 1 && !onlyNonFa) {
        this._logger.info(
          "No more things to scrape, fav instead.",
        );

        await this._favInsteadOfScrape();
      }

      return result;
    } catch (e) {
      this._logger.critical("Couldn't get next post. Failing.");
      this._logger.debug(e);

      Deno.exit(1);
    }
  }

  private async _checkExisting(
    originIdCombo: string,
  ): Promise<boolean | never> {
    let checkExistingResponse: CheckExistingOutput;

    try {
      checkExistingResponse = await this._gqlClient.checkExisting({
        postKey: originIdCombo,
      });

      return checkExistingResponse.queue_entry.length > 0;
    } catch (e) {
      this._logger.critical("Couldn't check if the post exists, failing.");
      this._logger.debug(e);

      Deno.exit(1);
    }
  }

  private async _scrapePostE6(
    id: number,
  ): Promise<ScrapeResult | ScrapeErrorResult | number> {
    try {
      const e6ResponseRaw = await fetch(`https://e621.net/posts/${id}.json`, {
        headers: {
          Authorization: `Basic ${
            btoa(`${this._e6Username}:${this._e6ApiKey}`)
          }`,
          'User-Agent': 'RadarsoftFaver/1.0'
        },
      });

      if (!e6ResponseRaw.ok) {
        const errorResult = {
          statusCode: e6ResponseRaw.status,
          statusText: e6ResponseRaw.statusText,
        } as { statusCode: number; statusText: string; body?: string };

        if (e6ResponseRaw.bodyUsed) {
          errorResult.body = await e6ResponseRaw.text();
        }

        return errorResult;
      }

      try {
        const e6ResponseJson = await e6ResponseRaw.json();

        if (e6ResponseJson.post?.flags?.deleted) {
          return {
            statusCode: 404,
            statusText: `Post ${id} marked as deleted.`,
          };
        }

        const fakeArtists = [
          "conditional_dnp",
          "avoid_posting",
          "third-party_edit",
        ];
        const profileName =
          ((e6ResponseJson.post?.tags?.artist as string[]) || []).find(
            (artist) => !fakeArtists.includes(artist),
          ) || "";

        const scraperResult: ScrapeResult = {
          link: `https://e621.net/posts/${id}`,
          title: `#${id} - e621`,
          profile_name: profileName,
          profile: `https://e621.net/artists/show_or_new?name=${profileName}`,
          full: e6ResponseJson.post?.file?.url,
          thumbnail: e6ResponseJson.post?.sample?.has
            ? e6ResponseJson.post?.sample?.url
            : e6ResponseJson.post?.file?.url,
        };

        return scraperResult;
      } catch (err) {
        this._logger.critical("Couldn't parse json from E621, failing.");
        this._logger.debug(err);

        return 1;
      }
    } catch (e) {
      this._logger.critical("Couldn't get response from E621, failing.");
      this._logger.debug(e);

      return 1;
    }
  }

  private async _scrapePostFa(
    id: number,
  ): Promise<ScrapeResult | ScrapeErrorResult | number> {
    let faScraperResponse: Response;
    try {
      faScraperResponse = await fetch(
        `${this._scraperUrl}/submission/${id}`,
        {
          method: "GET",
          headers: {
            "fa-cookie": this._faCookie,
          },
        },
      );
    } catch (e) {
      this._logger.critical(
        "Error fetching submission details from fa-scraper, exception follows.",
      );
      this._logger.debug(e);

      return 1;
    }

    if (!faScraperResponse.ok) {
      const errorResult: ScrapeErrorResult = {
        statusCode: faScraperResponse.status,
        statusText: faScraperResponse.statusText,
      };

      if (faScraperResponse.bodyUsed) {
        errorResult.body = await faScraperResponse.text();
      }

      if (faScraperResponse.status === 404) {
        errorResult.statusText =
          "Submission got deleted, removing from scraper queue table and jumping to the next entry.";
      }

      return errorResult;
    }

    const faScraperJson = await faScraperResponse.json() as ScrapeResult;

    this._logger.info(`Scraped data for PostID ${id}.`);
    this._logger.debug(faScraperJson);

    return faScraperJson;
  }

  private async _scrapePostWs(
    id: number,
  ): Promise<ScrapeResult | ScrapeErrorResult | number> {
    let wsResponse: Response;
    try {
      wsResponse = await fetch(
        `https://www.weasyl.com/api/submissions/${id}/view`,
        {
          method: "GET",
          headers: {
            "X-Weasyl-API-Key": this._wsApiKey,
          },
        },
      );
    } catch (e) {
      this._logger.critical(
        "Error fetching submission details from ws-scraper, exception follows.",
      );
      this._logger.debug(e);

      return 1;
    }

    if (!wsResponse.ok) {
      const errorResult: ScrapeErrorResult = {
        statusCode: wsResponse.status,
        statusText: wsResponse.statusText,
      };

      if (wsResponse.bodyUsed) {
        errorResult.body = await wsResponse.text();
      }

      if (wsResponse.status === 404) {
        errorResult.statusText =
          "Submission got deleted, removing from scraper queue table and jumping to the next entry.";
      }

      return errorResult;
    }

    const scraperJson = await wsResponse.json() as WeasylResult;

    if (
      scraperJson.type !== "submission" || scraperJson.subtype !== "visual" ||
      (!scraperJson.media.submission ||
        scraperJson.media.submission.length === 0)
    ) {
      const errorResult: ScrapeErrorResult = {
        statusCode: 415,
        statusText: `Weasyl post ${id} is not of supported media type.`,
        body: JSON.stringify(scraperJson, null, 2),
      };

      return errorResult;
    }

    const result: ScrapeResult = {
      full: scraperJson.media.submission[0].url,
      link: scraperJson.link,
      profile: `https://www.weasyl.com/~${scraperJson.owner_login}`,
      profile_name: scraperJson.owner_login,
      thumbnail: scraperJson.media.thumbnail[0].url,
      title: scraperJson.title,
    };

    return result;
  }

  private async _scrapePost(
    origin: PostOrigin,
    id: number,
  ): Promise<never | 0 | 404 | ScrapeResult> {
    let result: number | ScrapeResult | ScrapeErrorResult;
    switch (origin) {
      case "E6":
        result = await this._scrapePostE6(id);
        break;
      case "FA":
        result = await this._scrapePostFa(id);
        break;
      case "WE":
      case "WS":
        result = await this._scrapePostWs(id);
        break;
      default:
        result = {
          statusCode: 415,
          statusText: "Unsupported origin, remove and go next",
        };
    }

    if (typeof result === "number") {
      Deno.exit(result);
    }

    if ("statusCode" in result && "statusText" in result) {
      this._logger.critical(
        `Request for ${origin}_${id} failed with status code ${result.statusCode}: ${result.statusText}`,
      );

      if ("body" in result) {
        this._logger.debug(result.body);
      }

      if (result.statusCode === 404) {
        await this._deleteToScrape(`${origin}_${id}`);
        return 404;
      }

      if (this._retryCount < 3) {
        await new Promise((resolve) =>
          setTimeout(resolve, (this._retryCount + 1) * 5000)
        );
        this._retryCount++;
        return 0;
      }

      this._logger.critical(`Constant issue with ${origin}_${id}. Exiting.`);
      Deno.exit(1);
    }

    this._retryCount = 0;
    return result;
  }

  private async _checkReposter(
    artistName: string,
    postLink: string,
    id: number,
  ): Promise<boolean | never> {
    let checkReposterResponse: CheckReposterOutput;

    try {
      checkReposterResponse = await this._gqlClient.checkReposter({
        name: artistName,
      });

      if (checkReposterResponse.fa_reposters.length > 0) {
        await this._sendReposterMessage(postLink, id);
        await this._deleteToScrape(`FA_${id}`);
        return true;
      }

      return false;
    } catch (e) {
      this._logger.critical(
        "Couldn't check if the artist is reposter, failing.",
      );
      this._logger.debug(e);

      Deno.exit(1);
    }
  }

  private async _insertQueueEntry(
    postId: number,
    data: ScrapeResult,
    origin: PostOrigin,
  ): Promise<number | never> {
    const dataToInsert: InsertEntryInput = {
      apiVersion: this._apiVersion,
      full: data.full,
      link: data.link,
      origin: `${origin}`,
      postId,
      postKey: `${origin}_${postId}`,
      profile: data.profile,
      profileName: data.profile_name,
      thumbnail: data.thumbnail,
      title: data.title,
    };

    try {
      if (this._redisClient) {
        await this._redisClient.incr("to_save");
        await this._redisClient.incr("to_post");
      }

      const insertResult = await this._gqlClient.insertEntry(dataToInsert);
      return insertResult.insert_queue_entry_one.id;
    } catch (e) {
      this._logger.critical(
        `Couldn't insert queue entry "${dataToInsert.postKey}". Failing.`,
      );
      this._logger.debug(e);

      Deno.exit(1);
    }
  }

  private async _checkBlocked(profileName: string): Promise<boolean> {
    try {
      const checkBlockedResponse = await this._gqlClient.checkBlocked({
        profileName,
      });

      return checkBlockedResponse.fa_blocked.length > 0;
    } catch (e) {
      this._logger.critical(
        "Couldn't check if the person is blocked, check and maybe fav manually.",
      );
      this._logger.debug(e);

      return true;
    }
  }

  private async _favoriteE6(postId: number): Promise<void | number> {
    const e6RequestBody = {
      post_id: postId,
    };

    try {
      await fetch("https://e621.net/favorite.json", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          'User-Agent': 'RadarsoftFaver/1.0',
          Authorization: `Basic ${
            btoa(`${this._e6Username}:${this._e6ApiKey}`)
          }`
        },
        body: JSON.stringify(e6RequestBody),
      });
    } catch (e) {
      this._logger.critical(
        `Couldn't favorite E621 post: ${postId}. Do manually.`,
      );
      this._logger.debug(e);

      return 1;
    }
  }

  private async _favoriteFa(
    postId: number,
    favKey: string,
  ): Promise<void | number> {
    try {
      await fetch(`${this._scraperUrl}/submission/${postId}/favorite`, {
        method: "POST",
        body: JSON.stringify({
          fav_status: true,
          fav_key: favKey,
        }),
        headers: {
          "fa-cookie": this._faCookie,
          "Content-Type": "application/json",
        },
      });
    } catch (e) {
      this._logger.critical(
        `Couldn't favorite FA post: ${postId}. Do manually.`,
      );
      this._logger.debug(e);

      return 1;
    }
  }

  private async _favoriteWs(postId: number): Promise<void | number> {
    try {
      await fetch(`https://www.weasyl.com/api/submissions/${postId}/favorite`, {
        method: "POST",
        headers: {
          "X-Weasyl-API-Key": this._wsApiKey,
          "Content-Type": "application/json",
        },
      });
    } catch (e) {
      this._logger.critical(
        `Couldn't favorite Weasyl post: ${postId}. Do manually.`,
      );
      this._logger.debug(e);

      return 1;
    }
  }

  private async _favorite(
    origin: PostOrigin,
    postId: number,
    id: number,
    favKey?: string,
  ): Promise<void | never> {
    let result;
    switch (origin) {
      case "E6":
        result = await this._favoriteE6(postId);
        break;
      case "FA":
        if (!favKey) {
          result =
            `Couldn't favorite (supposedly) FA post: ${postId} - no 'fav_key' supplied!`;
          break;
        }

        result = await this._favoriteFa(postId, favKey);
        break;
      case "WS":
      case "WE":
        result = await this._favoriteWs(postId);
        break;
      default:
        result = `Unsupported origin for ${postId} - ${origin}`;
        break;
    }

    if (typeof result === "string") {
      this._logger.critical(result);
      Deno.exit(1);
    }

    if (typeof result === "number") {
      Deno.exit(result);
    }

    await this._gqlClient.setFaved(id);
  }

  private async _sendReposterMessage(
    link: string,
    postId: number,
  ): Promise<void | never> {
    try {
      await this._tgApiClient.sendMessage(
        this._userId,
        `Poster of submission ID <a href="${link}">${postId}</a> is possibly a reposter. Check and eventually add manually.`,
        {
          parse_mode: "HTML",
          link_preview_options: {
            is_disabled: true,
          },
        },
      );
    } catch (e) {
      this._logger.critical("Couldn't send reposter message to Telegram.");
      this._logger.debug(e);

      Deno.exit(1);
    }
  }

  public async run(): Promise<void | never> {
    try {
      const registeredUsers = await this._getStatus();
      const onlyFAToScrape = await this._gqlClient.checkNonFaToScrape();

      if (
        registeredUsers >= 10000 && Deno.env.get("IGNORE_LIMITS") === "false" &&
        onlyFAToScrape.fa_to_scrape_aggregate.aggregate.count === 0
      ) {
        this._logger.info("Too many registered users online, exiting.");
        Deno.exit(0);
      }

      this._logger.info("Trying to post something, while there's a space.");
      this._logger.info(
        "Starting timer for 3 minutes to not risk interfering with the next job instance.",
      );

      const endTime = new Date().setTime(
        new Date().getTime() + (3 * 60 * 1000),
      );

      // let i = 0
      // while (i === 0) {
      //   i++
      while (new Date().getTime() < endTime) {
        this._logger.info("Starting to scrape/import.");

        const nextPost = await this._getNextPost(registeredUsers > 10000);
        if (nextPost.fa_to_scrape.length === 0) {
          continue;
        }

        const { post_id, origin, ignore_reposter } = nextPost.fa_to_scrape[0];
        const originIdCombo = `${origin}_${post_id}`;

        const isExisting = await this._checkExisting(originIdCombo);

        if (isExisting) {
          this._logger.info(
            "Post apparently exists already, removing the line and continuing onto the next one.",
          );
          await this._deleteToScrape(originIdCombo);
          continue;
        }

        const postData = await this._scrapePost(origin, post_id);

        if (postData === 0 || postData === 404) {
          continue;
        }

        if (!ignore_reposter && origin === "FA") {
          if (
            await this._checkReposter(
              postData.profile_name,
              postData.link,
              post_id,
            )
          ) {
            continue;
          }
        }

        const newId = await this._insertQueueEntry(post_id, postData, origin);
        this._logger.info(`New entry inserted with ID ${newId}`);

        await this._deleteToScrape(originIdCombo);

        if (origin === "FA") {
          const isBlocked = await this._checkBlocked(postData.profile_name);
          this._logger.debug("isBlocked result:", isBlocked);
          if (isBlocked) {
            this._logger.info(
              `Artist ${postData.profile_name} is blocked, skipping favorite.`,
            );
            await this._gqlClient.setFaved(newId);
            continue;
          }
        }

        await this._favorite(origin, post_id, newId, postData.fav_key);
      }
    } catch (e) {
      this._logger.critical("Unexpected error happened.");
      this._logger.debug(e);

      Deno.exit(1);
    }
  }
}
