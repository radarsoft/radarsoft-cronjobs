const FA_SCRAPER_URL = Deno.env.get("FA_SCRAPER_URL");

if (!FA_SCRAPER_URL) {
  console.error("Scraper URL is unset!");
  Deno.exit(1);
}

try {
  await fetch(`${FA_SCRAPER_URL}/status`);
} catch (e) {
  console.error(e);
  Deno.exit(1);
}
