import { extname } from "path";
import { Api } from "grammy";
import { Message } from "types/grammy";
import { createLazyClient, Redis } from "redis";
import { GraphQlInit, TelegramApiInit } from "types";
import { GraphQl } from "graphql";
import { NextEntryOutput } from "types/graphql";
import { CronjobsLogger } from "../logger/index.ts";

export class TelegramPoster {
  private _gqlClient: GraphQl;
  private _tgApiClient: Api;
  private _channelId: string;
  private _logger: CronjobsLogger;
  private _redisClient?: Redis;

  constructor(
    channelId: string,
    gqlInit: GraphQlInit,
    tgApiInit: TelegramApiInit,
  ) {
    this._logger = new CronjobsLogger("PostToTelegram");
    this._channelId = channelId;
    this._gqlClient = new GraphQl(
      gqlInit.url,
      "X-Hasura-Admin-Secret",
      gqlInit.auth,
    );

    this._tgApiClient = new Api(tgApiInit.token, {
      apiRoot: tgApiInit.url,
    });

    const REDIS_HOST = Deno.env.get("redis-hostname");
    const REDIS_PORT = parseInt(Deno.env.get("redis-port") || "6379");
    const REDIS_PASSWORD = Deno.env.get("redis-password");
    if (!REDIS_HOST || !REDIS_PASSWORD) {
      this._logger.error(
        "Redis hostname and/or password not set, will NOT use redis.",
      );
    } else {
      this._redisClient = createLazyClient({
        hostname: REDIS_HOST,
        port: REDIS_PORT,
        password: REDIS_PASSWORD,
      });
    }
  }

  private async _getNextEntryData(): Promise<NextEntryOutput | never> {
    try {
      const result = await this._gqlClient.getNextEntry();
      if (result.get_next_post.length === 0) {
        this._logger.info("No more entries to post, quitting.");
        Deno.exit(0);
      }

      return result;
    } catch (e) {
      this._logger.critical("Couldn't get next post, failing.");
      this._logger.debug(e);

      Deno.exit(1);
    }
  }

  private async _postAnimation(
    url: string,
    caption: string,
  ): Promise<Message.AnimationMessage | false> {
    try {
      return await this._tgApiClient.sendAnimation(this._channelId, url, {
        caption,
        parse_mode: "HTML",
      });
    } catch (e) {
      this._logger.debug(e);
      return false;
    }
  }

  private async _postPhoto(
    url: string,
    caption: string,
  ): Promise<Message.PhotoMessage | false> {
    try {
      return await this._tgApiClient.sendPhoto(this._channelId, url, {
        caption,
        parse_mode: "HTML",
      });
    } catch (e) {
      this._logger.debug(e);
      return false;
    }
  }

  private async _postMessage(
    text: string,
  ): Promise<Message.TextMessage | false> {
    try {
      return await this._tgApiClient.sendMessage(this._channelId, text, {
        parse_mode: "HTML",
      });
    } catch (e) {
      this._logger.debug(e);
      return false;
    }
  }

  private async _postToTelegram(
    type: "message" | "photo" | "animation",
    text: string,
    url?: string,
  ): Promise<
    | Message.TextMessage
    | Message.PhotoMessage
    | Message.AnimationMessage
    | false
  > {
    switch (type) {
      case "animation":
        if (!url) {
          this._logger.critical("Couldn't post animation, url is not defined");
          return false;
        }
        return await this._postAnimation(url, text);
      case "message":
        return await this._postMessage(text);
      case "photo":
        if (!url) {
          this._logger.critical("Couldn't post photo, url is not defined");
          return false;
        }
        return await this._postPhoto(url, text);
    }
  }

  private async _setPosted(id: number): Promise<void | never> {
    try {
      if (this._redisClient) {
        await this._redisClient.decr("to_post");
      }

      await this._gqlClient.setPosted(id);
    } catch (e) {
      this._logger.critical(
        `Couldn't set posted to queue_entry with ID ${id}.`,
      );
      this._logger.debug(e);

      Deno.exit(1);
    }
  }

  private _buildCaption(
    postLink: string,
    postName: string,
    artistLink: string,
    fullLink: string,
    origin: string,
    artistName?: string,
    tipLink?: string,
  ): string {
    const chanceToKofi = Math.random() * 100;
    const kofi = chanceToKofi > 95;
    let originName = "";

    switch (origin) {
      case "WS":
      case "WE":
        originName = "Weasyl";
        break;
      case "SF":
        originName = "SoFurry";
        break;
      case "E6":
        originName = "e621";
        break;
      case "FA":
        originName = "Furaffinity";
        break;
      case "KE":
        originName = "Kemono";
        break;
      case "TG":
        originName = "Telegram";
        break;
      default:
        originName = "Unknown";
        break;
    }

    let caption = `<a href="${postLink}">${postName}</a> [${originName}]\n\n`;
    caption += `🖌: <a href="${encodeURI(artistLink)}">${
      artistName ? artistName : "Artist's profile"
    }</a>\n`;

    if (origin !== "TG") {
      caption += `🖼: <a href="${encodeURI(fullLink)}">Full resolution</a>\n`;
    }

    if (tipLink) {
      caption += `💲: <a href="${tipLink}">Tip the artist!</a>\n`;
    }

    caption +=
      `\n⭐️: <a href="https://t.me/RadarsPronz?boost">Boost this channel</a>\n`;

    if (kofi && !tipLink) {
      caption +=
        '<a href="https://ko-fi.com/D1D0WKOS">Support me on Ko-fi</a> | <a href="https://paypal.me/HueHueRadar">Paypal.Me</a>';
    }

    return caption;
  }

  public async post(): Promise<void | never> {
    try {
      const nextEntry = await this._getNextEntryData();

      const {
        id,
        full_link,
        artist_link,
        artist_name,
        post_link,
        post_name,
        tip_link,
        tg_image_link,
        origin,
      } = nextEntry.get_next_post[0];

      const postNameEscaped = post_name
        ? post_name.replace(/</g, "&lt;").replace(/>/g, "&gt;")
        : post_link;
      let sendType: "animation" | "photo" | "message" =
        extname(full_link) === ".gif" ? "animation" : "photo";

      let caption = this._buildCaption(
        post_link,
        postNameEscaped,
        artist_link,
        full_link,
        origin,
        artist_name,
        tip_link,
      );

      const firstTryResponse = await this._postToTelegram(
        sendType,
        caption,
        full_link,
      );

      if (firstTryResponse) {
        if (origin === "KE") {
          await this._gqlClient.updateApiVer(id);
        }

        this._logger.info(JSON.stringify(firstTryResponse, null, 2));
        await this._setPosted(id);
        Deno.exit(0);
      }

      sendType = extname(tg_image_link) === ".gif" ? "animation" : "photo";
      const secondTryResponse = await this._postToTelegram(
        sendType,
        caption,
        tg_image_link,
      );

      if (secondTryResponse) {
        if (origin === "KE") {
          await this._gqlClient.updateApiVer(id);
        }

        this._logger.info(JSON.stringify(secondTryResponse, null, 2));
        await this._setPosted(id);
        Deno.exit(0);
      }

      caption =
        `Unknown error when trying to post. Sorry for the inconvenience.\n\n${caption}`;
      sendType = "message";
      const lastTryResponse = await this._postToTelegram(sendType, caption);

      if (lastTryResponse) {
        if (origin === "KE") {
          await this._gqlClient.updateApiVer(id);
        }

        this._logger.info(JSON.stringify(lastTryResponse, null, 2));
        await this._setPosted(id);
        Deno.exit(0);
      }

      this._logger.critical(
        "Unknown error when trying to post image to telegram. Failure.",
      );
      Deno.exit(1);
    } catch (e) {
      this._logger.critical("Unexpected error occured, dump follows.");
      this._logger.debug(e);

      Deno.exit(1);
    }
  }
}
