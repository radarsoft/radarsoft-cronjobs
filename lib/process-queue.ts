import { QueueProcessor } from "./ProcessQueue/index.ts";

const FA_SCRAPER_URL = Deno.env.get("FA_SCRAPER_URL");
const FA_COOKIE_HEADER = Deno.env.get("FA_COOKIE_HEADER");
const GRAPHQL_URL = Deno.env.get("GQL_URL");
const GRAPHQL_AUTH = Deno.env.get("GQL_AUTH");
const API_VERSION = Deno.env.get("API_VERSION");
const USER_ID = Deno.env.get("TG_USER_CHAT_ID");
const BOT_TOKEN = Deno.env.get("BOT_API_TOKEN");
const TG_BOT_URL = Deno.env.get("TG_API_URL");
const E6_USERNAME = Deno.env.get("E6_USERNAME");
const E6_AUTH = Deno.env.get("E6_AUTH");
const WS_AUTH = Deno.env.get("WS_API_KEY");

if (
  !FA_SCRAPER_URL || !FA_COOKIE_HEADER || !GRAPHQL_URL || !GRAPHQL_AUTH ||
  !API_VERSION || !USER_ID || !BOT_TOKEN || !TG_BOT_URL || !E6_USERNAME ||
  !E6_AUTH || !WS_AUTH
) {
  console.error("One or more of the required env variables is unset!");
  Deno.exit(1);
}

const processor = new QueueProcessor(
  {
    url: GRAPHQL_URL,
    auth: GRAPHQL_AUTH,
  },
  {
    url: TG_BOT_URL,
    token: BOT_TOKEN,
  },
  FA_SCRAPER_URL,
  FA_COOKIE_HEADER,
  API_VERSION,
  USER_ID,
  E6_USERNAME,
  E6_AUTH,
  WS_AUTH,
);

await processor.run();
