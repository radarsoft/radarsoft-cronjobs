export type ScrapeResult = {
  link: string;
  title: string;
  profile_name: string;
  profile: string;
  full: string;
  thumbnail: string;
  fav_key?: string;
  fav_status?: boolean;
};
