export type RedisInit = {
  hostname: string;
  port: number;
  password: string;
};
