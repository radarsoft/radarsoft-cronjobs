export type WeasylMedia = {
  url: string;
  mediaid: number;
  links?: {
    [key: string]: WeasylMedia[];
  };
};
