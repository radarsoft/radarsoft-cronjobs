export type ScrapeErrorResult = {
  statusCode: number;
  statusText: string;
  body?: string;
};
