export type GraphQlInit = {
  url: string;
  auth?: string;
};
