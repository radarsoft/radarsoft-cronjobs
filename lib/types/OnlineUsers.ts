export type OnlineUsers = {
  guests: string | number;
  other: string | number;
  registered: string | number;
  total: string | number;
};
