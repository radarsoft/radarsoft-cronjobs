export type TelegramApiInit = {
  url: string;
  token: string;
};
