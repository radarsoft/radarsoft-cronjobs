import { WeasylMedia } from "types";

export type WeasylResult = {
  comments: number;
  description: string;
  embedlink?: string;
  favorited: boolean;
  favorites: number;
  folder_name?: string;
  folderid?: string;
  friends_only: boolean;
  owner: string;
  owner_login: string;
  owner_media: {
    avatar: WeasylMedia[];
    banner?: WeasylMedia[];
  };
  posted_at: string;
  rating: "general" | "moderate" | "mature" | "explicit";
  media: {
    thumbnail: WeasylMedia[];
    cover?: WeasylMedia[];
    submission?: WeasylMedia[];
  };
  submitid: number;
  subtype: "visual" | "literary" | "multimedia";
  tags: string[];
  title: string;
  type: "submission" | "character";
  views: number;
  link: string;
};
